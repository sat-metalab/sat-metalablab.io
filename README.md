# Portail pour dévs informatique

Portail pour développement informatique - [Société des arts technologiques](https://www.sat.qc.ca)

Pour accéder au portail: [https://sat-mtl.gitlab.io](https://sat-mtl.gitlab.io)

Pour nous suivre : [Twitter](https://www.twitter.com/SATmontreal)

Discuster avec nous: [Matrix - art tech hangout](https://matrix.to/#/!xIliUlonxNfrnwiTRv:matrix.org)

## to generate the diagrams

### requirements

#### for the static svg diagram
```
sudo apt install dvisvgm texlive-latex-base texlive-pictures sed
```

#### for the interactive data-driven svg diagram
```
sudo apt install npm sed
```

### script

#### for the static svg diagram

You need to compile the `.tex` file using the following command line instruction:

```
./generate_diagrams.sh
```

#### for the interactive data-driven svg diagram

##### install dependencies with node package manager
```
npm i
```

##### generate with webpack

2 options:
* if you want to develop the diagram with auto-reload:
```
npm run dev
```
* if you want to generate a bundle:
```
npm run build
```

Find more information about this diagram in [src/tools/README.md](src/tools/README.md).

